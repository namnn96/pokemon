import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { pages } from './pages';

@NgModule({
  declarations: [...pages],
  exports: [...pages],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ],
  providers: []
})
export class DashboardModule {}
