import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, take, takeUntil } from 'rxjs/operators';

import { PotterService } from '@app/features/+dashboard/services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit, OnDestroy {
  @ViewChild('search', { read: ElementRef })
  private searchElementRef: ElementRef;

  public selectedCharacter: any;
  public characters$: Observable<any[]>;
  public searchStream$: Subject<string> = new Subject<string>();
  public ngUnsubscribe$: Subject<void> = new Subject<void>();
  public showSuggestion: boolean = false;

  constructor(private _potterSvc: PotterService) { }

  ngOnInit(): void {
    this.characters$ = this._potterSvc.characters$;

    this.searchStream$.asObservable()
      .pipe(debounceTime(300), takeUntil(this.ngUnsubscribe$))
      .subscribe(searchText => {
        this.getCharacters(searchText);
      });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  getCharacters(searchText: string = ''): void {
    this._potterSvc.getCharacters(searchText).pipe(take(1)).subscribe();
  }

  searchKeyup($event: KeyboardEvent) {
    if ($event.keyCode !== 9) { // Avoid tab
      this.searchStream$.next(this.searchElementRef.nativeElement.value);
    }
  }

  searchFocus() {
    this.showSuggestion = true;
    this.searchStream$.next(this.searchElementRef.nativeElement.value);
  }

  selectCharacter(character: any) {
    this.selectedCharacter = character;
    this.showSuggestion = false;
  }
}
